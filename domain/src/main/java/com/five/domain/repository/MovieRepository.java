package com.five.domain.repository;

import com.five.domain.model.Movie;
import com.five.domain.model.MovieNote;
import com.five.domain.model.ScarceMovie;
import com.five.domain.model.SearchTerm;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface MovieRepository {

    Flowable<List<Movie>> getTrendingMovies();

    Flowable<List<Movie>> getFavoriteMovies();

    Flowable<MovieNote> getMovieNote(final String movieId);

    Flowable<Movie> getMovie(final String movieId);

    Completable fetchFreshData(final int offset);

    Completable fetchMovie(final String movieId);

    Single<List<ScarceMovie>> getSearchData(final SearchTerm searchTerm);

    Completable addToFavorites(final String movieId);

    Completable removeFromFavorites(final String movieId);

    Completable addMovieNote(final MovieNote movieNote);

    Completable deleteMovieNote(final String movieId);
}
