package com.five.domain.model;

import java.io.Serializable;

import lombok.Value;

@Value
public class Movie implements Serializable {

    public String id;

    public String name;

    public String synopsis;

    public String rating;

    public String thumbnailUrl;

    public String imageUrl;

    public boolean isFavorite;
}
