package com.five.domain.model;

import lombok.Value;

@Value
public class ScarceMovie {

    public String id;

    public String name;

    public String thumbnail;
}
