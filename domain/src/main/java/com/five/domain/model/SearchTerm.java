package com.five.domain.model;

import lombok.Value;

@Value
public class SearchTerm {

    private final String searchTerm;
    private final int limit;
    private final int offset;
}
