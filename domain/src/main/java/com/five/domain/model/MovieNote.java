package com.five.domain.model;

import lombok.Value;

@Value
public class MovieNote {

    public String movieId;

    public String noteTitle;

    public String note;
}
