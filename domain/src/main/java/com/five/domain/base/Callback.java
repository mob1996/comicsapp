package com.five.domain.base;

public interface Callback<T> {

    void onSuccess(T value);

    void onError(Throwable throwable);
}
