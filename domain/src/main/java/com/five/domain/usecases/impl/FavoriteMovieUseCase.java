package com.five.domain.usecases.impl;

import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.CommandUseCaseWithParams;

import io.reactivex.Completable;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FavoriteMovieUseCase implements CommandUseCaseWithParams<String> {

    private final MovieRepository movieRepository;

    @Override
    public Completable execute(final String movieId) {
        return movieRepository.addToFavorites(movieId);
    }
}
