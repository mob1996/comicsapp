package com.five.domain.usecases;

import io.reactivex.Single;

public interface GetUseCaseWithRequest<Request, Result> {

    Single<Result> get(Request request);
}
