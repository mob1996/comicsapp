package com.five.domain.usecases;

import io.reactivex.Flowable;

public interface QueryUseCase<Result> {

    Flowable<Result> run();
}