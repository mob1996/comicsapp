package com.five.domain.usecases;

import io.reactivex.Single;

public interface GetUseCase<Result> {

    Single<Result> get();
}
