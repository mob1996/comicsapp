package com.five.domain.usecases.impl;

import com.five.domain.model.Movie;
import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.QueryUseCase;

import java.util.List;

import io.reactivex.Flowable;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetTrendingMoviesUseCase implements QueryUseCase {

    private final MovieRepository movieRepository;

    @Override
    public Flowable<List<Movie>> run() {
        return movieRepository.getTrendingMovies();
    }
}
