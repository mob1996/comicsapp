package com.five.domain.usecases;

import io.reactivex.Completable;

public interface CommandUseCaseWithParams<Params> {

    Completable execute(Params params);
}