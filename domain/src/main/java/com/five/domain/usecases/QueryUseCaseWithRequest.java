package com.five.domain.usecases;

import io.reactivex.Flowable;

public interface QueryUseCaseWithRequest<Request, Result> {

    Flowable<Result> run(Request request);
}