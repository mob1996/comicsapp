package com.five.domain.usecases;

import io.reactivex.Completable;

public interface CommandUseCase {

    Completable execute();
}
