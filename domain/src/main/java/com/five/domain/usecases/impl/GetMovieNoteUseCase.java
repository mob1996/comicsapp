package com.five.domain.usecases.impl;

import com.five.domain.model.MovieNote;
import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.QueryUseCaseWithRequest;

import java.util.List;

import io.reactivex.Flowable;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetMovieNoteUseCase implements QueryUseCaseWithRequest<String, MovieNote>{

    private final MovieRepository movieRepository;

    @Override
    public Flowable<MovieNote> run(final String movieId) {
        return movieRepository.getMovieNote(movieId);
    }
}
