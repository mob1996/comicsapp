package com.five.domain.usecases.impl;

import com.five.domain.model.MovieNote;
import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.CommandUseCaseWithParams;

import io.reactivex.Completable;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AddMovieNoteUseCase implements CommandUseCaseWithParams<MovieNote> {

    private final MovieRepository movieRepository;

    @Override
    public Completable execute(final MovieNote movieNote) {
        return movieRepository.addMovieNote(movieNote);
    }
}
