package com.five.domain.usecases.impl;

import com.five.domain.model.Movie;
import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.QueryUseCaseWithRequest;

import io.reactivex.Flowable;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GetMovieUseCase implements QueryUseCaseWithRequest<String, Movie> {

    private final MovieRepository movieRepository;

    @Override
    public Flowable<Movie> run(final String movieId) {
        return movieRepository.getMovie(movieId);
    }
}
