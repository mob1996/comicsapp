package com.five.domain.usecases.impl;

import com.five.domain.model.ScarceMovie;
import com.five.domain.model.SearchTerm;
import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.GetUseCaseWithRequest;

import java.util.List;

import io.reactivex.Single;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SearchMoviesUseCase implements GetUseCaseWithRequest<SearchTerm, List<ScarceMovie>> {

    private final MovieRepository movieRepository;

    @Override
    public Single<List<ScarceMovie>> get(final SearchTerm searchTerm) {
        return movieRepository.getSearchData(searchTerm);
    }

}
