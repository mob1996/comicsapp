package com.five.domain.usecases.impl;

import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.CommandUseCaseWithParams;

import io.reactivex.Completable;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FetchMoviesUseCase implements CommandUseCaseWithParams<Integer> {

    private final MovieRepository movieRepository;

    @Override
    public Completable execute(final Integer offset) {
        return movieRepository.fetchFreshData(offset);
    }
}
