package com.five.data;

import com.five.data.local.movie.MovieDao;
import com.five.data.mappers.MovieMapper;
import com.five.data.local.movie.model.FavoriteMovie;
import com.five.data.mappers.MovieNoteMapper;
import com.five.data.mappers.ScarceMovieMapper;
import com.five.data.remote.movie.MovieService;
import com.five.domain.model.Movie;
import com.five.domain.model.MovieNote;
import com.five.domain.model.ScarceMovie;
import com.five.domain.model.SearchTerm;
import com.five.domain.repository.MovieRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MovieRepositoryImpl implements MovieRepository {

    private static final int LIMIT_MOVIES = 10;

    private final MovieService movieService;
    private final MovieDao movieDao;

    @Override
    public Flowable<List<Movie>> getTrendingMovies() {
        return Flowable.combineLatest(movieDao.getAllMovies(),
                                      movieDao.getFavoriteMovies(),
                                      MovieMapper::fromMovieEntities);
    }

    @Override
    public Flowable<List<Movie>> getFavoriteMovies() {
        return Flowable.combineLatest(movieDao.getFavoriteMovieEntities(),
                                      movieDao.getFavoriteMovies(),
                                      MovieMapper::fromMovieEntities);
    }

    @Override
    public Flowable<MovieNote> getMovieNote(final String movieId) {
        return movieDao.getMovieNotes(movieId)
                       .map(MovieNoteMapper::fromMovieNoteEntity);
    }

    @Override
    public Flowable<Movie> getMovie(final String movieId) {
        return Flowable.combineLatest(movieDao.getMovie(movieId),
                                      movieDao.isFavorite(movieId),
                                      MovieMapper::fromMovieEntity
        );
    }

    @Override
    public Completable fetchFreshData(final int offset) {
        return movieService.getTrendingMovies(LIMIT_MOVIES, offset)
                           .map(movieResult -> MovieMapper.fromMovieResponses(movieResult.getMovieResponses()))
                           .flatMapCompletable(movieEntities -> Completable.fromAction(() -> movieDao.addMovies(movieEntities)));
    }

    @Override
    public Completable fetchMovie(final String movieId) {
        return movieService.getMovie(movieId)
                           .map(singleMovieResult -> MovieMapper.fromMovieResponse(singleMovieResult.getResponse()))
                           .flatMapCompletable(movieEntity -> Completable.fromAction(() -> movieDao.addMovie(movieEntity)));
    }

    @Override
    public Single<List<ScarceMovie>> getSearchData(final SearchTerm searchTerm) {
        String actualSearchTerm = String.format("name:%s,description:%s", searchTerm.getSearchTerm(), searchTerm.getSearchTerm());
        return movieService.searchMovies(actualSearchTerm, searchTerm.getLimit(), searchTerm.getOffset())
                           .map(scarceMovieResult -> ScarceMovieMapper.fromScarceMovieResponses(scarceMovieResult.getScarceMovieResponses()));
    }

    @Override
    public Completable addToFavorites(final String movieId) {
        return Completable.fromAction(() -> movieDao.setFavoriteMovie(new FavoriteMovie(movieId)));
    }

    @Override
    public Completable removeFromFavorites(final String movieId) {
        return Completable.fromAction(() -> movieDao.removeFavoriteMovie(new FavoriteMovie(movieId)));
    }

    @Override
    public Completable addMovieNote(final MovieNote movieNote) {
        return Completable.fromAction(() -> movieDao.addMovieNote(MovieNoteMapper.fromMovieNote(movieNote)));
    }

    @Override
    public Completable deleteMovieNote(final String movieId) {
        return Completable.fromAction(() -> movieDao.deleteMovieNote(movieId));
    }
}
