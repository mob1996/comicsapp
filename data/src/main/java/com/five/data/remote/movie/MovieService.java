package com.five.data.remote.movie;

import com.five.data.remote.movie.model.MovieResult;
import com.five.data.remote.movie.model.ScarceMovieResult;
import com.five.data.remote.movie.model.SingleMovieResult;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieService {

    @GET("api/movies/")
    Single<MovieResult> getTrendingMovies(@Query("limit") final int limit,
                                          @Query("offset") final int offset);

    @GET("api/movies/")
    Single<ScarceMovieResult> searchMovies(@Query("filter") final String searchTerm,
                                           @Query("limit") final int limit,
                                           @Query("offset") final int offset);

    @GET("api/movie/4025-{id}/")
    Single<SingleMovieResult> getMovie(@Path("id") final String movieId);
}
