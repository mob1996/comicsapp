package com.five.data.remote.movie.model;

import com.google.gson.annotations.SerializedName;

import lombok.Value;

@Value
public class SingleMovieResult {

    @SerializedName("results")
    private final MovieResponse response;
}
