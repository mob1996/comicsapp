package com.five.data.remote.movie.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Value;

@Value
public class MovieResponse implements Serializable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("description")
    public String synopsis;

    @SerializedName("rating")
    public String rating;

    @SerializedName("image")
    public MovieImages movieImages;

    @Value
    public static class MovieImages implements Serializable {

        @SerializedName("thumb_url")
        public String thumbnail;

        @SerializedName("super_url")
        public String image;
    }
}
