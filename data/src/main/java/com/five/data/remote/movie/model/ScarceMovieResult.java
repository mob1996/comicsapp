package com.five.data.remote.movie.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Value;

@Value
public class ScarceMovieResult {

    @SerializedName("results")
    public List<ScarceMovieResponse> scarceMovieResponses;
}
