package com.five.data.remote.movie.model;

import com.google.gson.annotations.SerializedName;

import lombok.Value;

@Value
public class ScarceMovieResponse {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("image")
    public MovieThumbnail movieThumbnail;

    @Value
    public static class MovieThumbnail {

        @SerializedName("thumb_url")
        public String thumbnail;
    }
}
