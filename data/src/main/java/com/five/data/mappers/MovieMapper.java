package com.five.data.mappers;

import com.annimon.stream.Stream;
import com.five.data.local.movie.model.FavoriteMovie;
import com.five.data.local.movie.model.MovieEntity;
import com.five.data.remote.movie.model.MovieResponse;
import com.five.domain.model.Movie;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MovieMapper {

    public static Movie fromMovieResponse(final MovieResponse movieResponse, final boolean isFavorite) {
        return new Movie(movieResponse.getId(),
                         movieResponse.getName(),
                         movieResponse.getSynopsis(),
                         movieResponse.getRating(),
                         movieResponse.getMovieImages().getThumbnail(),
                         movieResponse.getMovieImages().getImage(),
                         isFavorite);
    }

    public static List<Movie> fromMovieResponses(final List<MovieResponse> movieResponses, final List<FavoriteMovie> favorites) {
        final Set<String> favoriteMoviesStr = fromFavoriteMovies(favorites);
        return Stream.of(movieResponses)
                     .map(movieResponse -> fromMovieResponse(movieResponse, favoriteMoviesStr.contains(movieResponse.getId())))
                     .toList();
    }

    public static Movie fromMovieEntity(final MovieEntity movieEntity, final boolean isFavorite) {
        return new Movie(movieEntity.getId(),
                         movieEntity.getName(),
                         movieEntity.getSynopsis(),
                         movieEntity.getRating(),
                         movieEntity.getThumbnailUrl(),
                         movieEntity.getImageUrl(),
                         isFavorite);
    }

    public static List<Movie> fromMovieEntities(final List<MovieEntity> movieEntities, final List<FavoriteMovie> favorites) {
        final Set<String> favoriteMoviesStr = fromFavoriteMovies(favorites);
        return Stream.of(movieEntities)
                     .map(movieEntity -> fromMovieEntity(movieEntity, favoriteMoviesStr.contains(movieEntity.getId())))
                     .toList();
    }

    public static MovieEntity fromMovie(final Movie movie) {
        return new MovieEntity(movie.getId(),
                               movie.getName(),
                               movie.getSynopsis(),
                               movie.getRating(),
                               movie.getThumbnailUrl(),
                               movie.getImageUrl());
    }

    public static List<MovieEntity> fromMovies(final List<Movie> movies) {
        return Stream.of(movies)
                     .map(MovieMapper::fromMovie)
                     .toList();
    }

    public static MovieEntity fromMovieResponse(final MovieResponse movieResponse) {
        return new MovieEntity(movieResponse.getId(),
                               movieResponse.getName(),
                               movieResponse.getSynopsis(),
                               movieResponse.getRating(),
                               movieResponse.getMovieImages().getThumbnail(),
                               movieResponse.getMovieImages().getImage());
    }

    public static List<MovieEntity> fromMovieResponses(final List<MovieResponse> movieResponses) {
        return Stream.of(movieResponses)
                     .map(MovieMapper::fromMovieResponse)
                     .toList();
    }

    public static Set<String> fromFavoriteMovies(final List<FavoriteMovie> favoriteMovies) {
        return new TreeSet<>(Stream.of(favoriteMovies)
                                   .map(favoriteMovie -> favoriteMovie.favoriteId)
                                   .toList());
    }
}
