package com.five.data.mappers;

import com.annimon.stream.Stream;
import com.five.data.local.movie.model.MovieNoteEntity;
import com.five.domain.model.MovieNote;

import java.util.List;

public class MovieNoteMapper {

    public static MovieNote fromMovieNoteEntity(final MovieNoteEntity movieNoteEntity) {
        return new MovieNote(movieNoteEntity.getMovieId(),
                             movieNoteEntity.getNoteTitle(),
                             movieNoteEntity.getNote());
    }

    public static List<MovieNote> fromMovieNoteEntities(final List<MovieNoteEntity> movieNoteEntities) {
        return Stream.of(movieNoteEntities)
                     .map(MovieNoteMapper::fromMovieNoteEntity)
                     .toList();
    }

    public static MovieNoteEntity fromMovieNote(final MovieNote movieNote) {
        return new MovieNoteEntity(movieNote.getMovieId(),
                                   movieNote.getNoteTitle(),
                                   movieNote.getNote());
    }

    public static List<MovieNoteEntity> fromMovieNotes(final List<MovieNote> movieNotes) {
        return Stream.of(movieNotes)
                     .map(MovieNoteMapper::fromMovieNote)
                     .toList();
    }
}
