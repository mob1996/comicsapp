package com.five.data.mappers;

import com.annimon.stream.Stream;
import com.five.data.remote.movie.model.ScarceMovieResponse;
import com.five.domain.model.ScarceMovie;

import java.util.List;

public class ScarceMovieMapper {

    public static ScarceMovie fromScarceMovieResponse(ScarceMovieResponse scarceMovieResponse) {
        return new ScarceMovie(scarceMovieResponse.getId(),
                               scarceMovieResponse.getName(),
                               scarceMovieResponse.getMovieThumbnail().getThumbnail());
    }

    public static List<ScarceMovie> fromScarceMovieResponses(List<ScarceMovieResponse> scarceMovieResponses) {
        return Stream.of(scarceMovieResponses)
                     .map(ScarceMovieMapper::fromScarceMovieResponse)
                     .toList();
    }
}
