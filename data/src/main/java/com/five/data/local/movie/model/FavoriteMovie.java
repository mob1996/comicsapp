package com.five.data.local.movie.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class FavoriteMovie {

    @PrimaryKey
    @NonNull
    public final String favoriteId;

    public FavoriteMovie(@NonNull final String favoriteId) {
        this.favoriteId = favoriteId;
    }
}