package com.five.data.local.movie.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

import lombok.Value;

@Value
@Entity(tableName = "movie")
public class MovieEntity implements Serializable {

    @PrimaryKey
    @NonNull
    public String id;

    public String name;

    public String synopsis;

    public String rating;

    public String thumbnailUrl;

    public String imageUrl;

    public MovieEntity(@NonNull final String id, final String name, final String synopsis, final String rating, final String thumbnailUrl, final String imageUrl) {
        this.id = id;
        this.name = name;
        this.synopsis = synopsis;
        this.rating = rating;
        this.thumbnailUrl = thumbnailUrl;
        this.imageUrl = imageUrl;
    }
}
