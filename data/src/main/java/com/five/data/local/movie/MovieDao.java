package com.five.data.local.movie;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.five.data.local.movie.model.FavoriteMovie;
import com.five.data.local.movie.model.MovieEntity;
import com.five.data.local.movie.model.MovieNoteEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface MovieDao {

    @Query("SELECT EXISTS (SELECT favoriteId FROM favoriteMovie WHERE (:movieId) = favoriteId)")
    Flowable<Boolean> isFavorite(final String movieId);

    @Query("SELECT * FROM movie WHERE id = (:movieId)")
    Flowable<MovieEntity> getMovie(final String movieId);

    @Query("SELECT * FROM movie")
    Flowable<List<MovieEntity>> getAllMovies();

    @Query("SELECT * FROM favoritemovie")
    Flowable<List<FavoriteMovie>> getFavoriteMovies();

    @Query("SELECT id, name, synopsis, rating, thumbnailUrl, imageUrl FROM movie JOIN favoritemovie WHERE movie.id = favoritemovie.favoriteId")
    Flowable<List<MovieEntity>> getFavoriteMovieEntities();

    @Query("SELECT * FROM note WHERE movieId = (:movieId)")
    Flowable<MovieNoteEntity> getMovieNotes(final String movieId);

    @Query("DELETE FROM movie")
    void deleteMovies();

    @Query("DELETE FROM note WHERE movieId = (:movieId)")
    void deleteMovieNote(final String movieId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addMovies(final List<MovieEntity> movieEntities);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addMovie(final MovieEntity movieEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addMovieNote(final MovieNoteEntity movieNoteEntity);

    @Insert
    void setFavoriteMovie(final FavoriteMovie favoriteMovie);

    @Delete
    void removeFavoriteMovie(final FavoriteMovie favoriteMovie);
}
