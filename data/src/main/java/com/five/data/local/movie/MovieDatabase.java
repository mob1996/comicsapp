package com.five.data.local.movie;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.five.data.local.movie.model.FavoriteMovie;
import com.five.data.local.movie.model.MovieEntity;
import com.five.data.local.movie.model.MovieNoteEntity;

@Database(entities = {MovieEntity.class, FavoriteMovie.class, MovieNoteEntity.class}, version = 1, exportSchema = false)
public abstract class MovieDatabase extends RoomDatabase {

    public abstract MovieDao movieDao();
}
