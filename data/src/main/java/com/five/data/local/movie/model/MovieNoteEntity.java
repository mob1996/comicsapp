package com.five.data.local.movie.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import lombok.Data;

@Data
@Entity(tableName = "note")
public class MovieNoteEntity {

    @NonNull
    @PrimaryKey
    public final String movieId;

    public final String noteTitle;

    public final String note;

    public MovieNoteEntity(final String movieId, final String noteTitle, final String note) {
        this.movieId = movieId;
        this.noteTitle = noteTitle;
        this.note = note;
    }
}
