package com.five.data.local.movie;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.RxRoom;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import com.five.data.local.movie.model.FavoriteMovie;
import com.five.data.local.movie.model.MovieEntity;
import com.five.data.local.movie.model.MovieNoteEntity;
import io.reactivex.Flowable;
import java.lang.Boolean;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings("unchecked")
public class MovieDao_Impl implements MovieDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMovieEntity;

  private final EntityInsertionAdapter __insertionAdapterOfMovieNoteEntity;

  private final EntityInsertionAdapter __insertionAdapterOfFavoriteMovie;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfFavoriteMovie;

  private final SharedSQLiteStatement __preparedStmtOfDeleteMovies;

  private final SharedSQLiteStatement __preparedStmtOfDeleteMovieNote;

  public MovieDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMovieEntity = new EntityInsertionAdapter<MovieEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `movie`(`id`,`name`,`synopsis`,`rating`,`thumbnailUrl`,`imageUrl`) VALUES (?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MovieEntity value) {
        if (value.id == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.id);
        }
        if (value.name == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.name);
        }
        if (value.synopsis == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.synopsis);
        }
        if (value.rating == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.rating);
        }
        if (value.thumbnailUrl == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.thumbnailUrl);
        }
        if (value.imageUrl == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.imageUrl);
        }
      }
    };
    this.__insertionAdapterOfMovieNoteEntity = new EntityInsertionAdapter<MovieNoteEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `note`(`movieId`,`noteTitle`,`note`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MovieNoteEntity value) {
        if (value.movieId == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.movieId);
        }
        if (value.noteTitle == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.noteTitle);
        }
        if (value.note == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.note);
        }
      }
    };
    this.__insertionAdapterOfFavoriteMovie = new EntityInsertionAdapter<FavoriteMovie>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `FavoriteMovie`(`favoriteId`) VALUES (?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FavoriteMovie value) {
        if (value.favoriteId == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.favoriteId);
        }
      }
    };
    this.__deletionAdapterOfFavoriteMovie = new EntityDeletionOrUpdateAdapter<FavoriteMovie>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `FavoriteMovie` WHERE `favoriteId` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, FavoriteMovie value) {
        if (value.favoriteId == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.favoriteId);
        }
      }
    };
    this.__preparedStmtOfDeleteMovies = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM movie";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteMovieNote = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM note WHERE movieId = (?)";
        return _query;
      }
    };
  }

  @Override
  public void addMovies(final List<MovieEntity> movieEntities) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMovieEntity.insert(movieEntities);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void addMovie(final MovieEntity movieEntity) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMovieEntity.insert(movieEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void addMovieNote(final MovieNoteEntity movieNoteEntity) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMovieNoteEntity.insert(movieNoteEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void setFavoriteMovie(final FavoriteMovie favoriteMovie) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfFavoriteMovie.insert(favoriteMovie);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void removeFavoriteMovie(final FavoriteMovie favoriteMovie) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfFavoriteMovie.handle(favoriteMovie);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteMovies() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteMovies.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteMovies.release(_stmt);
    }
  }

  @Override
  public void deleteMovieNote(final String movieId) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteMovieNote.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (movieId == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, movieId);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteMovieNote.release(_stmt);
    }
  }

  @Override
  public Flowable<Boolean> isFavorite(final String movieId) {
    final String _sql = "SELECT EXISTS (SELECT favoriteId FROM favoriteMovie WHERE (?) = favoriteId)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (movieId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, movieId);
    }
    return RxRoom.createFlowable(__db, new String[]{"favoriteMovie"}, new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final Boolean _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp == null ? null : _tmp != 0;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<MovieEntity> getMovie(final String movieId) {
    final String _sql = "SELECT * FROM movie WHERE id = (?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (movieId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, movieId);
    }
    return RxRoom.createFlowable(__db, new String[]{"movie"}, new Callable<MovieEntity>() {
      @Override
      public MovieEntity call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfName = _cursor.getColumnIndexOrThrow("name");
          final int _cursorIndexOfSynopsis = _cursor.getColumnIndexOrThrow("synopsis");
          final int _cursorIndexOfRating = _cursor.getColumnIndexOrThrow("rating");
          final int _cursorIndexOfThumbnailUrl = _cursor.getColumnIndexOrThrow("thumbnailUrl");
          final int _cursorIndexOfImageUrl = _cursor.getColumnIndexOrThrow("imageUrl");
          final MovieEntity _result;
          if(_cursor.moveToFirst()) {
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpSynopsis;
            _tmpSynopsis = _cursor.getString(_cursorIndexOfSynopsis);
            final String _tmpRating;
            _tmpRating = _cursor.getString(_cursorIndexOfRating);
            final String _tmpThumbnailUrl;
            _tmpThumbnailUrl = _cursor.getString(_cursorIndexOfThumbnailUrl);
            final String _tmpImageUrl;
            _tmpImageUrl = _cursor.getString(_cursorIndexOfImageUrl);
            _result = new MovieEntity(_tmpId,_tmpName,_tmpSynopsis,_tmpRating,_tmpThumbnailUrl,_tmpImageUrl);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<List<MovieEntity>> getAllMovies() {
    final String _sql = "SELECT * FROM movie";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"movie"}, new Callable<List<MovieEntity>>() {
      @Override
      public List<MovieEntity> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfName = _cursor.getColumnIndexOrThrow("name");
          final int _cursorIndexOfSynopsis = _cursor.getColumnIndexOrThrow("synopsis");
          final int _cursorIndexOfRating = _cursor.getColumnIndexOrThrow("rating");
          final int _cursorIndexOfThumbnailUrl = _cursor.getColumnIndexOrThrow("thumbnailUrl");
          final int _cursorIndexOfImageUrl = _cursor.getColumnIndexOrThrow("imageUrl");
          final List<MovieEntity> _result = new ArrayList<MovieEntity>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final MovieEntity _item;
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpSynopsis;
            _tmpSynopsis = _cursor.getString(_cursorIndexOfSynopsis);
            final String _tmpRating;
            _tmpRating = _cursor.getString(_cursorIndexOfRating);
            final String _tmpThumbnailUrl;
            _tmpThumbnailUrl = _cursor.getString(_cursorIndexOfThumbnailUrl);
            final String _tmpImageUrl;
            _tmpImageUrl = _cursor.getString(_cursorIndexOfImageUrl);
            _item = new MovieEntity(_tmpId,_tmpName,_tmpSynopsis,_tmpRating,_tmpThumbnailUrl,_tmpImageUrl);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<List<FavoriteMovie>> getFavoriteMovies() {
    final String _sql = "SELECT * FROM favoritemovie";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"favoritemovie"}, new Callable<List<FavoriteMovie>>() {
      @Override
      public List<FavoriteMovie> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfFavoriteId = _cursor.getColumnIndexOrThrow("favoriteId");
          final List<FavoriteMovie> _result = new ArrayList<FavoriteMovie>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final FavoriteMovie _item;
            final String _tmpFavoriteId;
            _tmpFavoriteId = _cursor.getString(_cursorIndexOfFavoriteId);
            _item = new FavoriteMovie(_tmpFavoriteId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<List<MovieEntity>> getFavoriteMovieEntities() {
    final String _sql = "SELECT id, name, synopsis, rating, thumbnailUrl, imageUrl FROM movie JOIN favoritemovie WHERE movie.id = favoritemovie.favoriteId";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"movie","favoritemovie"}, new Callable<List<MovieEntity>>() {
      @Override
      public List<MovieEntity> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfName = _cursor.getColumnIndexOrThrow("name");
          final int _cursorIndexOfSynopsis = _cursor.getColumnIndexOrThrow("synopsis");
          final int _cursorIndexOfRating = _cursor.getColumnIndexOrThrow("rating");
          final int _cursorIndexOfThumbnailUrl = _cursor.getColumnIndexOrThrow("thumbnailUrl");
          final int _cursorIndexOfImageUrl = _cursor.getColumnIndexOrThrow("imageUrl");
          final List<MovieEntity> _result = new ArrayList<MovieEntity>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final MovieEntity _item;
            final String _tmpId;
            _tmpId = _cursor.getString(_cursorIndexOfId);
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpSynopsis;
            _tmpSynopsis = _cursor.getString(_cursorIndexOfSynopsis);
            final String _tmpRating;
            _tmpRating = _cursor.getString(_cursorIndexOfRating);
            final String _tmpThumbnailUrl;
            _tmpThumbnailUrl = _cursor.getString(_cursorIndexOfThumbnailUrl);
            final String _tmpImageUrl;
            _tmpImageUrl = _cursor.getString(_cursorIndexOfImageUrl);
            _item = new MovieEntity(_tmpId,_tmpName,_tmpSynopsis,_tmpRating,_tmpThumbnailUrl,_tmpImageUrl);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<MovieNoteEntity> getMovieNotes(final String movieId) {
    final String _sql = "SELECT * FROM note WHERE movieId = (?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (movieId == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, movieId);
    }
    return RxRoom.createFlowable(__db, new String[]{"note"}, new Callable<MovieNoteEntity>() {
      @Override
      public MovieNoteEntity call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfMovieId = _cursor.getColumnIndexOrThrow("movieId");
          final int _cursorIndexOfNoteTitle = _cursor.getColumnIndexOrThrow("noteTitle");
          final int _cursorIndexOfNote = _cursor.getColumnIndexOrThrow("note");
          final MovieNoteEntity _result;
          if(_cursor.moveToFirst()) {
            final String _tmpMovieId;
            _tmpMovieId = _cursor.getString(_cursorIndexOfMovieId);
            final String _tmpNoteTitle;
            _tmpNoteTitle = _cursor.getString(_cursorIndexOfNoteTitle);
            final String _tmpNote;
            _tmpNote = _cursor.getString(_cursorIndexOfNote);
            _result = new MovieNoteEntity(_tmpMovieId,_tmpNoteTitle,_tmpNote);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
