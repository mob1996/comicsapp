package com.five.data.local.movie;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class MovieDatabase_Impl extends MovieDatabase {
  private volatile MovieDao _movieDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `movie` (`id` TEXT NOT NULL, `name` TEXT, `synopsis` TEXT, `rating` TEXT, `thumbnailUrl` TEXT, `imageUrl` TEXT, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `FavoriteMovie` (`favoriteId` TEXT NOT NULL, PRIMARY KEY(`favoriteId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `note` (`movieId` TEXT NOT NULL, `noteTitle` TEXT, `note` TEXT, PRIMARY KEY(`movieId`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"62bab62335cbf8a40d3dae5e5d77a99a\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `movie`");
        _db.execSQL("DROP TABLE IF EXISTS `FavoriteMovie`");
        _db.execSQL("DROP TABLE IF EXISTS `note`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsMovie = new HashMap<String, TableInfo.Column>(6);
        _columnsMovie.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsMovie.put("name", new TableInfo.Column("name", "TEXT", false, 0));
        _columnsMovie.put("synopsis", new TableInfo.Column("synopsis", "TEXT", false, 0));
        _columnsMovie.put("rating", new TableInfo.Column("rating", "TEXT", false, 0));
        _columnsMovie.put("thumbnailUrl", new TableInfo.Column("thumbnailUrl", "TEXT", false, 0));
        _columnsMovie.put("imageUrl", new TableInfo.Column("imageUrl", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMovie = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMovie = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMovie = new TableInfo("movie", _columnsMovie, _foreignKeysMovie, _indicesMovie);
        final TableInfo _existingMovie = TableInfo.read(_db, "movie");
        if (! _infoMovie.equals(_existingMovie)) {
          throw new IllegalStateException("Migration didn't properly handle movie(com.five.data.local.movie.model.MovieEntity).\n"
                  + " Expected:\n" + _infoMovie + "\n"
                  + " Found:\n" + _existingMovie);
        }
        final HashMap<String, TableInfo.Column> _columnsFavoriteMovie = new HashMap<String, TableInfo.Column>(1);
        _columnsFavoriteMovie.put("favoriteId", new TableInfo.Column("favoriteId", "TEXT", true, 1));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFavoriteMovie = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFavoriteMovie = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFavoriteMovie = new TableInfo("FavoriteMovie", _columnsFavoriteMovie, _foreignKeysFavoriteMovie, _indicesFavoriteMovie);
        final TableInfo _existingFavoriteMovie = TableInfo.read(_db, "FavoriteMovie");
        if (! _infoFavoriteMovie.equals(_existingFavoriteMovie)) {
          throw new IllegalStateException("Migration didn't properly handle FavoriteMovie(com.five.data.local.movie.model.FavoriteMovie).\n"
                  + " Expected:\n" + _infoFavoriteMovie + "\n"
                  + " Found:\n" + _existingFavoriteMovie);
        }
        final HashMap<String, TableInfo.Column> _columnsNote = new HashMap<String, TableInfo.Column>(3);
        _columnsNote.put("movieId", new TableInfo.Column("movieId", "TEXT", true, 1));
        _columnsNote.put("noteTitle", new TableInfo.Column("noteTitle", "TEXT", false, 0));
        _columnsNote.put("note", new TableInfo.Column("note", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysNote = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesNote = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoNote = new TableInfo("note", _columnsNote, _foreignKeysNote, _indicesNote);
        final TableInfo _existingNote = TableInfo.read(_db, "note");
        if (! _infoNote.equals(_existingNote)) {
          throw new IllegalStateException("Migration didn't properly handle note(com.five.data.local.movie.model.MovieNoteEntity).\n"
                  + " Expected:\n" + _infoNote + "\n"
                  + " Found:\n" + _existingNote);
        }
      }
    }, "62bab62335cbf8a40d3dae5e5d77a99a", "d7723e0fc763fc3f8a0e3c6c3c5290bf");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "movie","FavoriteMovie","note");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `movie`");
      _db.execSQL("DELETE FROM `FavoriteMovie`");
      _db.execSQL("DELETE FROM `note`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public MovieDao movieDao() {
    if (_movieDao != null) {
      return _movieDao;
    } else {
      synchronized(this) {
        if(_movieDao == null) {
          _movieDao = new MovieDao_Impl(this);
        }
        return _movieDao;
      }
    }
  }
}
