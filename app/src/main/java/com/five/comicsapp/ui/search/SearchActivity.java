package com.five.comicsapp.ui.search;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.five.comicsapp.R;
import com.five.comicsapp.util.ActivityUtils;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class SearchActivity extends DaggerAppCompatActivity {

    @Inject
    SearchFragment searchFragment;

    @Inject
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ActivityUtils.addFragmentToActivity(fragmentManager, searchFragment, R.id.search_frame);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
