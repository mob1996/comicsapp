package com.five.comicsapp.ui.addeditnote;

import com.five.comicsapp.base.BaseView;
import com.five.comicsapp.base.ViewPresenter;

public interface AddEditNoteContract {

    interface View extends BaseView<AddEditNotePresenter> {



    }

    interface Presenter extends ViewPresenter<View> {

    }
}
