package com.five.comicsapp.ui.search;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import com.five.comicsapp.di.scopes.ActivityScope;
import com.five.comicsapp.di.scopes.FragmentScope;
import com.five.domain.usecases.impl.FetchMovieUseCase;
import com.five.domain.usecases.impl.SearchMoviesUseCase;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import io.reactivex.processors.PublishProcessor;

@Module
public abstract class SearchModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract SearchFragment searchFragment();

    @ActivityScope
    @Provides
    static FragmentManager fragmentManager(final SearchActivity searchActivity) {
        return searchActivity.getSupportFragmentManager();
    }

    @ActivityScope
    @Provides
    static ActionBar actionBar(final SearchActivity searchActivity) {
        return searchActivity.getSupportActionBar();
    }

    @ActivityScope
    @Binds
    abstract SearchContract.Presenter searchPresenter(final SearchPresenter searchPresenter);
}
