package com.five.comicsapp.ui.frontpage.holder;

import android.support.v7.util.DiffUtil;

import com.five.domain.model.Movie;

import java.util.List;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MoviesDiffCallback extends DiffUtil.Callback {

    private final List<Movie> oldMovies;
    private final List<Movie> newMovies;

    @Override
    public int getOldListSize() {
        return oldMovies.size();
    }

    @Override
    public int getNewListSize() {
        return newMovies.size();
    }

    @Override
    public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
        return oldMovies.get(oldItemPosition).getId().equals(newMovies.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
        return oldMovies.get(oldItemPosition).isFavorite() == newMovies.get(newItemPosition).isFavorite();
    }
}
