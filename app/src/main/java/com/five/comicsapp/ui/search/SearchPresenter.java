package com.five.comicsapp.ui.search;


import com.annimon.stream.Optional;
import com.five.comicsapp.base.BasePresenter;
import com.five.comicsapp.base.Response;
import com.five.domain.model.ScarceMovie;
import com.five.domain.model.SearchTerm;
import com.five.domain.usecases.impl.FetchMovieUseCase;
import com.five.domain.usecases.impl.SearchMoviesUseCase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SearchPresenter extends BasePresenter<SearchContract.View> implements SearchContract.Presenter {

    private final SearchMoviesUseCase searchMoviesUseCase;
    private final FetchMovieUseCase fetchMovieUseCase;

    private final int throttle = 600;

    private final int defaultLimit = 10;
    private final int defaultOffset = 0;
    private int offset = defaultOffset;

    @Inject
    public SearchPresenter(final SearchMoviesUseCase searchMoviesUseCase,
                           final FetchMovieUseCase fetchMovieUseCase) {
        this.searchMoviesUseCase = searchMoviesUseCase;
        this.fetchMovieUseCase = fetchMovieUseCase;
    }

    @Override
    public void onStart() {
        addDisposable(view.searchTerm()
                          .subscribeOn(Schedulers.io())
                          .throttleWithTimeout(throttle, TimeUnit.MILLISECONDS)
                          .filter(searchTerm -> searchTerm.length() >= 3)
                          .flatMapSingle(searchTerm -> searchMoviesUseCase.get(new SearchTerm(searchTerm, defaultLimit, defaultOffset))
                                                                          .map(scarceMovies -> new Response<>(Optional.of(scarceMovies),
                                                                                                              Optional.empty()))
                                                                          .onErrorReturn(Response::withError))
                          .observeOn(AndroidSchedulers.mainThread())
                          .subscribe(response -> {
                              if(response.getValue().isPresent()) {
                                  List<ScarceMovie> scarceMovies = response.getValue().get();
                                  if(scarceMovies.size() > 0) {
                                      view.hideNoResultMessage();
                                      view.showMovies(scarceMovies);
                                  } else {
                                    view.clear();
                                    view.showNoResultMessage();
                                  }
                                  offset = defaultOffset;
                              } else {
                                  view.clear();
                                  view.showNoResultMessage();
                              }

                          }, Throwable::printStackTrace));
    }

    @Override
    public void onStop() {

    }

    @Override
    public void movieDetails(final String movieId) {
        addDisposable(fetchMovieUseCase.execute(movieId)
                                       .subscribeOn(Schedulers.io())
                                       .observeOn(AndroidSchedulers.mainThread())
                                       .subscribe(() -> view.showMovieDetails(movieId), Throwable::printStackTrace));
    }

    @Override
    public void loadMore() {
        offset += defaultLimit;
        addDisposable(searchMoviesUseCase.get(new SearchTerm(view.currentSearchTerm(), defaultLimit, offset))
                                         .subscribeOn(Schedulers.io())
                                         .observeOn(AndroidSchedulers.mainThread())
                                         .subscribe(view::addMovies, Throwable::printStackTrace));
    }
}
