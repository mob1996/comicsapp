package com.five.comicsapp.ui.moviedetails.details;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.os.Bundle;

import com.five.comicsapp.R;
import com.five.comicsapp.util.ActivityUtils;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class MovieDetailsActivity extends DaggerAppCompatActivity {

    @Inject
    MovieDetailsFragment movieDetailsFragment;

    @Inject
    FragmentManager fragmentManager;

    @Inject
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        ActivityUtils.addFragmentToActivity(fragmentManager, movieDetailsFragment, R.id.movie_details_frame);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
