package com.five.comicsapp.ui.addeditnote;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import com.five.comicsapp.R;
import com.five.comicsapp.util.ActivityUtils;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class AddEditNoteActivity extends DaggerAppCompatActivity {

    @Inject
    FragmentManager fragmentManager;

    @Inject
    ActionBar actionBar;

    @Inject
    AddEditNoteFragment addEditNoteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);
        ActivityUtils.addFragmentToActivity(fragmentManager, addEditNoteFragment, R.id.add_edit_note_frame);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
