package com.five.comicsapp.ui.frontpage.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.function.BiConsumer;
import com.five.comicsapp.R;
import com.five.comicsapp.util.Constants;
import com.five.comicsapp.ui.frontpage.holder.MoviesRecycleViewAdapter;
import com.five.comicsapp.ui.moviedetails.details.MovieDetailsActivity;
import com.five.comicsapp.util.Consumer;
import com.five.domain.model.Movie;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import dagger.android.support.DaggerFragment;

public class FrontPageFragment extends DaggerFragment implements FrontPageContract.View {

    @BindView(R.id.movies_recycle_view)
    RecyclerView moviesRecycleView;

    @BindView(R.id.swipe_refresh_movie)
    SwipeRefreshLayout movieSwipeRefreshLayout;

    private MoviesRecycleViewAdapter moviesRecycleViewAdapter;

    @Inject
    FrontPageContract.Presenter presenter;

    @Inject
    public FrontPageFragment() {
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final BiConsumer<String, View> showDetailsConsumer = (movieId, view) -> presenter.movieDetails(movieId, view);
        final Consumer<Movie> favoriteConsumer = movie -> presenter.favorite(movie);

        moviesRecycleViewAdapter = new MoviesRecycleViewAdapter(showDetailsConsumer, favoriteConsumer);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_front_page, container, false);
        ButterKnife.bind(this, root);

        movieSwipeRefreshLayout.setOnRefreshListener(() -> presenter.fetchMovies());

        moviesRecycleView.setHasFixedSize(true);
        final RecyclerView.ItemAnimator animator = moviesRecycleView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        moviesRecycleView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        moviesRecycleView.setAdapter(moviesRecycleViewAdapter);
        moviesRecycleView.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (!v.canScrollVertically(Constants.SCROLL_DOWN)) {
                presenter.fetchMovies();
            }
        });

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.start(this);
    }

    @Override
    public void onStop() {
        presenter.stop();
        super.onStop();
    }

    @Override
    public void showMovies(final List<Movie> movie) {
        moviesRecycleViewAdapter.updateMovies(movie);
    }

    @Override
    public void stopRefreshing() {
        movieSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void scrollToTop() {
        moviesRecycleView.smoothScrollToPosition(0);
    }

    @Override
    public int itemShown() {
        return moviesRecycleViewAdapter.getItemCount();
    }

    @Override
    public void showMovieDetails(final String movieId, final View view) {
        final Intent movieDetailsIntent = new Intent(this.getContext(), MovieDetailsActivity.class);
        movieDetailsIntent.putExtra(Constants.IntentKeys.EXTRA_MOVIE_ID, movieId);

        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this.getActivity(),
                view,
                ViewCompat.getTransitionName(view)
        );

        startActivity(movieDetailsIntent, optionsCompat.toBundle());
    }
}
