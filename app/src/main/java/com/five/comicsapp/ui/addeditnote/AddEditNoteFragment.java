package com.five.comicsapp.ui.addeditnote;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.five.comicsapp.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import dagger.android.support.DaggerFragment;

public class AddEditNoteFragment extends DaggerFragment implements AddEditNoteContract.View {

    @BindView(R.id.add_edit_note_title)
    EditText noteTitleEditText;

    @BindView(R.id.add_edit_note_content)
    EditText noteContentEditText;

    @BindView(R.id.add_edit_note_save_button)
    Button saveButton;

    @Inject
    AddEditNoteContract.Presenter presenter;

    @Inject
    public AddEditNoteFragment() {
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_add_edit_note, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.start(this);
    }

    @Override
    public void onStop() {
        presenter.stop();
        super.onStop();
    }
}
