package com.five.comicsapp.ui.frontpage.favorite;

import android.util.Log;
import android.view.View;

import com.five.comicsapp.base.BasePresenter;
import com.five.domain.model.Movie;
import com.five.domain.usecases.impl.GetFavoriteMoviesUseCase;
import com.five.domain.usecases.impl.UnfavoriteMovieUseCase;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.functions.Functions;
import io.reactivex.schedulers.Schedulers;

public class FrontPageFavoritePresenter extends BasePresenter<FrontPageFavoriteContract.View> implements FrontPageFavoriteContract.Presenter {

    private final GetFavoriteMoviesUseCase getFavoriteMoviesUseCase;
    private final UnfavoriteMovieUseCase unfavoriteMovieUseCase;

    @Inject
    public FrontPageFavoritePresenter(final GetFavoriteMoviesUseCase getFavoriteMoviesUseCase,
                                      final UnfavoriteMovieUseCase unfavoriteMovieUseCase) {
        this.getFavoriteMoviesUseCase = getFavoriteMoviesUseCase;
        this.unfavoriteMovieUseCase = unfavoriteMovieUseCase;
    }

    @Override
    public void onStart() {
        addDisposable(getFavoriteMoviesUseCase.run()
                                              .subscribeOn(Schedulers.io())
                                              .observeOn(AndroidSchedulers.mainThread())
                                              .subscribe(movies -> {
                                                  view.showMovies(movies);
                                                  setMessageVisibility();
                                              }, Throwable::printStackTrace));
    }

    @Override
    public void onStop() {

    }

    @Override
    public void movieDetails(final String movieId, final View view) {
        this.view.showMovieDetails(movieId, view);
    }

    @Override
    public void favorite(final Movie movie) {
        addDisposable(unfavoriteMovieUseCase.execute(movie.getId())
                                            .subscribeOn(Schedulers.io())
                                            .subscribe(this::setMessageVisibility, Throwable::printStackTrace));
    }

    private void setMessageVisibility() {
        if(view.getItemCount() > 0) {
            view.hideEmptyMessage();
        } else {
            view.showEmptyMessage();
        }
    }
}
