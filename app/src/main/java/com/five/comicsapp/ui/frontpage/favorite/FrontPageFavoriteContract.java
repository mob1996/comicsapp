package com.five.comicsapp.ui.frontpage.favorite;


import com.five.comicsapp.base.BaseView;
import com.five.comicsapp.base.ViewPresenter;
import com.five.domain.model.Movie;

import java.util.List;

public interface FrontPageFavoriteContract {

    interface View extends BaseView<FrontPageFavoriteContract.Presenter> {

        void showMovies(final List<Movie> movies);

        void showMovieDetails(final String movieId, final android.view.View view);

        void showEmptyMessage();

        void hideEmptyMessage();

        int getItemCount();
    }

    interface Presenter extends ViewPresenter<View>{

        void movieDetails(final String movieId, final android.view.View view);

        void favorite(final Movie movie);

    }
}
