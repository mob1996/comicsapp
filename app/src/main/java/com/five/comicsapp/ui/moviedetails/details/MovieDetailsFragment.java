package com.five.comicsapp.ui.moviedetails.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.motion.MotionLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.five.comicsapp.R;
import com.five.comicsapp.animation.Animator;
import com.five.comicsapp.util.Constants;
import com.five.domain.model.Movie;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import dagger.android.support.DaggerFragment;

public class MovieDetailsFragment extends DaggerFragment implements MovieDetailsContract.View {

    @BindView(R.id.movie_details_favorite)
    Button favoriteBtn;

    @BindView(R.id.movie_details_image)
    ImageView movieImage;

    @BindView(R.id.movie_details_name)
    TextView movieName;

    @BindView(R.id.movie_details_rating)
    TextView movieRating;

    @BindView(R.id.movie_details_synopsis)
    TextView movieSynopsis;

    @BindView(R.id.movie_details_scroll_view)
    ScrollView scrollView;

    @BindView(R.id.movie_details_inner_constraint_layout)
    MotionLayout motionLayout;

    @Inject
    MovieDetailsContract.Presenter presenter;

    @Inject
    String movieId;

    private final float favoriteBtnEndScale = 1.15f;
    private final int favoriteBtnAnimationHalfTime = 250;

    private boolean isFavorite;

    @Inject
    public MovieDetailsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_movie_details, container, false);
        ButterKnife.bind(this, root);
        movieImage.setTransitionName(movieId);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.start(this);
    }

    @Override
    public void onStop() {
        presenter.stop();
        super.onStop();
    }

    @Override
    public void render(final MovieDetailsContract.MovieViewModel viewModel) {
        final Movie movie = viewModel.getMovie();
        final boolean isExpanded = viewModel.isExpanded();
        isFavorite = movie.isFavorite();
        Glide.with(this).load(movie.getImageUrl()).into(movieImage);

        movieName.setText(movie.getName());
        movieRating.setText(movie.getRating());
        movieSynopsis.setText(movie.getSynopsis());
        favoriteBtn.setBackgroundResource(movie.isFavorite() ? R.drawable.favorite : R.drawable.not_favorite);
        if (isExpanded) {
            motionLayout.transitionToEnd();
        } else {
            motionLayout.transitionToStart();
        }
    }

    @Override
    public void animateFavoriteAction() {
        Animator.popAnimation(favoriteBtn, favoriteBtnEndScale, favoriteBtnAnimationHalfTime);
    }

    @Override
    public String getMovieId() {
        return movieId;
    }

    @OnClick(R.id.movie_details_favorite)
    public void submitFavorite(final View view) {
        presenter.favorite(isFavorite, movieId);
    }

    @OnClick(R.id.movie_details_image)
    public void scaleImage(final View view) {
        presenter.imageClicked();
    }


}
