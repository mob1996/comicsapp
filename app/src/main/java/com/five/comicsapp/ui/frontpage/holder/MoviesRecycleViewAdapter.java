package com.five.comicsapp.ui.frontpage.holder;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.annimon.stream.function.BiConsumer;
import com.bumptech.glide.Glide;
import com.five.comicsapp.R;
import com.five.comicsapp.animation.Animator;
import com.five.comicsapp.util.Consumer;
import com.five.domain.model.Movie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesRecycleViewAdapter extends RecyclerView.Adapter<MoviesRecycleViewAdapter.ViewHolder> {

    private final List<Movie> movies = new ArrayList<>();
    private final BiConsumer<String, View> viewMovieConsumer;
    private final Consumer<Movie> favoriteConsumer;

    public MoviesRecycleViewAdapter(final BiConsumer<String, View> viewMovieConsumer, final Consumer<Movie> favoriteConsumer) {
        this.viewMovieConsumer = viewMovieConsumer;
        this.favoriteConsumer = favoriteConsumer;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(view, viewMovieConsumer, favoriteConsumer);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Movie movie = movies.get(position);
        holder.render(movie);
    }

    public void updateMovies(final List<Movie> newMovies) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MoviesDiffCallback(movies, newMovies));
        this.movies.clear();
        this.movies.addAll(newMovies);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_thumbnail)
        ImageView movieThumbnailView;

        @BindView(R.id.movie_name)
        TextView movieNameView;

        @BindView(R.id.movie_synopsis)
        TextView movieSynopsisView;

        @BindView(R.id.movie_rating)
        TextView movieRatingView;

        @BindView(R.id.button_favorite)
        Button favoriteBtn;

        private final BiConsumer<String, View> viewMovieConsumer;
        private final Consumer<Movie> favoriteConsumer;

        ViewHolder(final View itemView, final BiConsumer<String, View> viewMovieConsumer, final Consumer<Movie> favoriteConsumer) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.viewMovieConsumer = viewMovieConsumer;
            this.favoriteConsumer = favoriteConsumer;
        }

        private void render(final Movie movie) {
            ViewCompat.setTransitionName(movieThumbnailView, movie.getId());

            movieNameView.setText(movie.getName());
            movieRatingView.setText(movie.getRating());
            movieSynopsisView.setText(movie.getSynopsis());
            favoriteBtn.setBackgroundResource(movie.isFavorite() ? R.drawable.favorite : R.drawable.not_favorite);
            Glide.with(itemView)
                 .load(movie.getThumbnailUrl())
                 .into(movieThumbnailView);

            itemView.setOnClickListener(v -> viewMovieConsumer.accept(movie.getId(), movieThumbnailView));
            favoriteBtn.setOnClickListener(v -> {
                favoriteConsumer.accept(movie);
                Animator.popAnimation(favoriteBtn, 1.1f, 250);
            });
        }
    }
}
