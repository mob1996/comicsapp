package com.five.comicsapp.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.five.comicsapp.R;
import com.five.comicsapp.ui.moviedetails.details.MovieDetailsActivity;
import com.five.comicsapp.ui.search.holder.ScarceMovieRecycleViewAdapter;
import com.five.comicsapp.util.Constants;
import com.five.comicsapp.util.Consumer;
import com.five.comicsapp.util.TextWatcherAdapter;
import com.five.domain.model.ScarceMovie;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;

import dagger.android.support.DaggerFragment;

public class SearchFragment extends DaggerFragment implements SearchContract.View {

    @BindView(R.id.search_toolbar)
    Toolbar toolbar;

    @BindView(R.id.search_edit_text)
    EditText searchEditText;

    @BindView(R.id.search_recycle_view)
    RecyclerView searchRecycleView;

    @BindView(R.id.search_message)
    TextView searchMessage;

    private final PublishProcessor<String> searchTermProcessor = PublishProcessor.create();

    @Inject
    SearchContract.Presenter presenter;

    private ScarceMovieRecycleViewAdapter scarceMovieRecycleViewAdapter;

    @Inject
    public SearchFragment() {
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Consumer<String> showDetailsConsumer = movieId -> presenter.movieDetails(movieId);

        scarceMovieRecycleViewAdapter = new ScarceMovieRecycleViewAdapter(showDetailsConsumer);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, root);

        setUpToolBar();
        setUpRecyclerView();

        searchEditText.addTextChangedListener(new TextWatcherAdapter() {

            @Override
            public void afterTextChanged(final Editable s) {
                searchTermProcessor.onNext(s.toString());
            }
        });
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.start(this);
    }

    @Override
    public void onStop() {
        presenter.stop();
        super.onStop();
    }

    @Override
    public void showMovieDetails(final String movieId) {
        final Intent movieDetailsIntent = new Intent(this.getContext(), MovieDetailsActivity.class);
        movieDetailsIntent.putExtra(Constants.IntentKeys.EXTRA_MOVIE_ID, movieId);
        startActivity(movieDetailsIntent);
    }

    @Override
    public void showMovies(final List<ScarceMovie> scarceMovies) {
        scarceMovieRecycleViewAdapter.updateScarceMovies(scarceMovies);
    }

    @Override
    public void addMovies(final List<ScarceMovie> scarceMovies) {
        scarceMovieRecycleViewAdapter.addScarceMovies(scarceMovies);
    }

    @Override
    public void clear() {
        scarceMovieRecycleViewAdapter.clearMovies();
    }

    @Override
    public void showNoResultMessage() {
        searchMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoResultMessage() {
        searchMessage.setVisibility(View.INVISIBLE);
    }

    @Override
    public Flowable<String> searchTerm() {
        return searchTermProcessor;
    }

    @Override
    public String currentSearchTerm() {
        return searchEditText.getText().toString();
    }

    private void setUpToolBar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void setUpRecyclerView() {
        searchRecycleView.setHasFixedSize(true);
        final RecyclerView.ItemAnimator animator = searchRecycleView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        searchRecycleView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        searchRecycleView.setAdapter(scarceMovieRecycleViewAdapter);
        searchRecycleView.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (!v.canScrollVertically(Constants.SCROLL_DOWN)) {
                presenter.loadMore();
            }
        });
    }
}
