package com.five.comicsapp.ui.moviedetails.details;

import com.five.comicsapp.base.BaseView;
import com.five.comicsapp.base.ViewPresenter;
import com.five.domain.model.Movie;

import lombok.Value;

public interface MovieDetailsContract {

    interface View extends BaseView<MovieDetailsPresenter> {

        void render(final MovieViewModel viewModel);

        String getMovieId();

        void animateFavoriteAction();
    }

    interface Presenter extends ViewPresenter<View> {

        void favorite(final boolean isFavorite, final String movieId);

        void imageClicked();
    }

    @Value
    class MovieViewModel {

        private final Movie movie;

        private final boolean isExpanded;
    }
}
