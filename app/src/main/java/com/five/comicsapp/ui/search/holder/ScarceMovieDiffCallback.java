package com.five.comicsapp.ui.search.holder;

import android.support.v7.util.DiffUtil;

import com.five.domain.model.ScarceMovie;

import java.util.List;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ScarceMovieDiffCallback extends DiffUtil.Callback {

    private final List<ScarceMovie> oldScarceMovies;
    private final List<ScarceMovie> newScarceMovies;

    @Override
    public int getOldListSize() {
        return oldScarceMovies.size();
    }

    @Override
    public int getNewListSize() {
        return newScarceMovies.size();
    }

    @Override
    public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
        return oldScarceMovies.get(oldItemPosition).getId().equals(
                newScarceMovies.get(newItemPosition).getId()
        );
    }

    @Override
    public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
        return oldScarceMovies.get(oldItemPosition).getId().equals(
                newScarceMovies.get(newItemPosition).getId()
        );
    }
}
