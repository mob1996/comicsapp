package com.five.comicsapp.ui.addeditnote;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import com.five.comicsapp.di.scopes.ActivityScope;
import com.five.comicsapp.di.scopes.FragmentScope;
import com.five.domain.usecases.impl.AddMovieNoteUseCase;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddEditNoteModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract AddEditNoteFragment addEditNoteFragment();

    @ActivityScope
    @Provides
    static FragmentManager fragmentManager(final AddEditNoteActivity addEditNoteActivity) {
        return addEditNoteActivity.getSupportFragmentManager();
    }

    @ActivityScope
    @Provides
    static ActionBar actionBar(final AddEditNoteActivity addEditNoteActivity) {
        return addEditNoteActivity.getSupportActionBar();
    }

    @ActivityScope
    @Binds
    abstract AddEditNoteContract.Presenter addEditNotePresenter(final AddEditNotePresenter addEditNotePresenter);
}
