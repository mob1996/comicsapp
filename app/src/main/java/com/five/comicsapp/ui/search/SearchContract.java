package com.five.comicsapp.ui.search;

import com.five.comicsapp.base.BaseView;
import com.five.comicsapp.base.ViewPresenter;
import com.five.domain.model.ScarceMovie;

import java.util.List;

import io.reactivex.Flowable;

public interface SearchContract {

    interface View extends BaseView<SearchPresenter> {

        void showMovieDetails(String movieId);

        void showMovies(List<ScarceMovie> scarceMovies);

        void addMovies(List<ScarceMovie> scarceMovies);

        void clear();

        void showNoResultMessage();

        void hideNoResultMessage();

        Flowable<String> searchTerm();

        String currentSearchTerm();
    }

    interface Presenter extends ViewPresenter<View> {

        void movieDetails(String movieId);

        void loadMore();
    }
}
