package com.five.comicsapp.ui.frontpage.movies;

import android.view.View;

import com.five.comicsapp.base.BasePresenter;
import com.five.domain.model.Movie;
import com.five.domain.usecases.impl.FavoriteMovieUseCase;
import com.five.domain.usecases.impl.FetchMoviesUseCase;
import com.five.domain.usecases.impl.GetTrendingMoviesUseCase;
import com.five.domain.usecases.impl.UnfavoriteMovieUseCase;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.functions.Functions;
import io.reactivex.schedulers.Schedulers;

public class FrontPagePresenter extends BasePresenter<FrontPageContract.View> implements FrontPageContract.Presenter {

    private final GetTrendingMoviesUseCase getTrendingMoviesUseCase;
    private final FavoriteMovieUseCase favoriteMovieUseCase;
    private final UnfavoriteMovieUseCase unfavoriteMovieUseCase;
    private final FetchMoviesUseCase fetchMoviesUseCase;

    @Inject
    public FrontPagePresenter(final GetTrendingMoviesUseCase getTrendingMoviesUseCase,
                              final FavoriteMovieUseCase favoriteMovieUseCase,
                              final UnfavoriteMovieUseCase unfavoriteMovieUseCase,
                              final FetchMoviesUseCase fetchMoviesUseCase) {
        this.getTrendingMoviesUseCase = getTrendingMoviesUseCase;
        this.favoriteMovieUseCase = favoriteMovieUseCase;
        this.unfavoriteMovieUseCase = unfavoriteMovieUseCase;
        this.fetchMoviesUseCase = fetchMoviesUseCase;
    }

    @Override
    public void fetchMovies() {
        addDisposable(fetchMoviesUseCase.execute(view.itemShown())
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(view::stopRefreshing, t -> {
                                            view.stopRefreshing();
                                            t.printStackTrace();
                                        })
        );
    }

    @Override
    public void movieDetails(final String movieId, final View view) {
        this.view.showMovieDetails(movieId, view);
    }

    @Override
    public void favorite(final Movie movie) {
        if (movie.isFavorite()) {
            addDisposable(unfavoriteMovieUseCase.execute(movie.getId())
                                                .subscribeOn(Schedulers.io())
                                                .subscribe(Functions.EMPTY_ACTION, Throwable::printStackTrace));
        } else {
            addDisposable(favoriteMovieUseCase.execute(movie.getId())
                                              .subscribeOn(Schedulers.io())
                                              .subscribe(Functions.EMPTY_ACTION, Throwable::printStackTrace));
        }
    }

    @Override
    public void onStart() {
        fetchMovies();
        addDisposable(getTrendingMoviesUseCase.run()
                                              .subscribeOn(Schedulers.io())
                                              .observeOn(AndroidSchedulers.mainThread())
                                              .subscribe(movies -> {
                                                  view.showMovies(movies);
                                                  view.stopRefreshing();
                                              }, Throwable::printStackTrace));
    }

    @Override
    public void onStop() {

    }
}
