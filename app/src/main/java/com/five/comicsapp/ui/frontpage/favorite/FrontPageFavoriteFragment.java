package com.five.comicsapp.ui.frontpage.favorite;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.function.BiConsumer;
import com.five.comicsapp.R;
import com.five.comicsapp.ui.frontpage.holder.MoviesRecycleViewAdapter;
import com.five.comicsapp.ui.moviedetails.details.MovieDetailsActivity;
import com.five.comicsapp.util.Constants;
import com.five.comicsapp.util.Consumer;
import com.five.domain.model.Movie;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import dagger.android.support.DaggerFragment;

public class FrontPageFavoriteFragment extends DaggerFragment implements FrontPageFavoriteContract.View {

    @BindView(R.id.movies_favorite_recycle_view)
    RecyclerView moviesRecycleView;

    @BindView(R.id.movies_favorite_empty_message)
    TextView emptyMessage;

    private MoviesRecycleViewAdapter moviesRecycleViewAdapter;

    @Inject
    FrontPageFavoriteContract.Presenter presenter;

    @Inject
    public FrontPageFavoriteFragment() {
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final BiConsumer<String, View> showDetailsConsumer = (movieId, view) -> presenter.movieDetails(movieId, view);
        final Consumer<Movie> favoriteConsumer = movie -> presenter.favorite(movie);

        moviesRecycleViewAdapter = new MoviesRecycleViewAdapter(showDetailsConsumer, favoriteConsumer);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_front_page_favorite, container, false);
        ButterKnife.bind(this, root);

        moviesRecycleView.setHasFixedSize(true);
        moviesRecycleView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        RecyclerView.ItemAnimator animator = moviesRecycleView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        moviesRecycleView.setAdapter(moviesRecycleViewAdapter);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.start(this);
    }

    @Override
    public void onStop() {
        presenter.stop();
        super.onStop();
    }

    @Override
    public void showMovies(final List<Movie> movie) {
        moviesRecycleViewAdapter.updateMovies(movie);
    }

    @Override
    public void showMovieDetails(final String movieId, final View view) {
        final Intent movieDetailsIntent = new Intent(this.getContext(), MovieDetailsActivity.class);
        movieDetailsIntent.putExtra(Constants.IntentKeys.EXTRA_MOVIE_ID, movieId);

        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this.getActivity(),
                view,
                ViewCompat.getTransitionName(view)
        );
        startActivity(movieDetailsIntent, optionsCompat.toBundle());
    }

    @Override
    public void showEmptyMessage() {
        emptyMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyMessage() {
        emptyMessage.setVisibility(View.INVISIBLE);

    }

    @Override
    public int getItemCount() {
        return moviesRecycleViewAdapter.getItemCount();
    }
}
