package com.five.comicsapp.ui.search.holder;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.five.comicsapp.R;
import com.five.comicsapp.util.Consumer;
import com.five.domain.model.ScarceMovie;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScarceMovieRecycleViewAdapter extends RecyclerView.Adapter<ScarceMovieRecycleViewAdapter.ViewHolder> {

    private final List<ScarceMovie> scarceMovies = new ArrayList<>();
    private final Consumer<String> viewMovieDetails;

    public ScarceMovieRecycleViewAdapter(final Consumer<String> viewMovieDetails) {
        this.viewMovieDetails = viewMovieDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.item_scarce_movie, parent, false);
        return new ViewHolder(view, viewMovieDetails);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ScarceMovie scarceMovie = scarceMovies.get(position);
        holder.render(scarceMovie);
    }

    @Override
    public int getItemCount() {
        return scarceMovies.size();
    }

    public void updateScarceMovies(final List<ScarceMovie> newScarceMovies) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ScarceMovieDiffCallback(this.scarceMovies,
                                                                                            newScarceMovies));
        this.scarceMovies.clear();
        this.scarceMovies.addAll(newScarceMovies);
        diffResult.dispatchUpdatesTo(this);
    }

    public void addScarceMovies(final List<ScarceMovie> scarceMovies) {
        final List<ScarceMovie> copyScarceMovies = new ArrayList<>(this.scarceMovies);
        this.scarceMovies.addAll(scarceMovies);

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ScarceMovieDiffCallback(copyScarceMovies,
                                                                                            this.scarceMovies));
        diffResult.dispatchUpdatesTo(this);
    }

    public void clearMovies() {
        final List<ScarceMovie> copyScarceMovies = new ArrayList<>(this.scarceMovies);
        this.scarceMovies.clear();
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new ScarceMovieDiffCallback(copyScarceMovies,
                                                                                            scarceMovies));
        diffResult.dispatchUpdatesTo(this);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.scarce_movie_thumbnail)
        ImageView scarceMovieThumbnailView;

        @BindView(R.id.scarce_movie_name)
        TextView scarceMovieNameView;

        private final Consumer<String> viewMovieDetails;

        public ViewHolder(final View itemView, final Consumer<String> viewMovieDetails) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.viewMovieDetails = viewMovieDetails;
        }

        private void render(final ScarceMovie scarceMovie) {
            scarceMovieNameView.setText(scarceMovie.getName());
            Glide.with(itemView)
                 .load(scarceMovie.getThumbnail())
                 .into(scarceMovieThumbnailView);
            itemView.setOnClickListener(v -> viewMovieDetails.accept(scarceMovie.getId()));
        }
    }
}
