package com.five.comicsapp.ui.frontpage.movies;

import com.five.comicsapp.base.BaseView;
import com.five.comicsapp.base.ViewPresenter;
import com.five.domain.model.Movie;

import java.util.List;

public interface FrontPageContract {

    interface View extends BaseView<FrontPagePresenter> {

        void showMovies(final List<Movie> movies);

        void stopRefreshing();

        void showMovieDetails(final String movieId, final android.view.View view);

        void scrollToTop();

        int itemShown();
    }

    interface Presenter extends ViewPresenter<View> {

        void fetchMovies();

        void movieDetails(final String movieId, final android.view.View view);

        void favorite(final Movie movie);
    }
}
