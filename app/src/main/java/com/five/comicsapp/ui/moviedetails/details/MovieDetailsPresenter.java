package com.five.comicsapp.ui.moviedetails.details;

import com.five.comicsapp.base.BasePresenter;
import com.five.domain.usecases.impl.FavoriteMovieUseCase;
import com.five.domain.usecases.impl.GetMovieUseCase;
import com.five.domain.usecases.impl.UnfavoriteMovieUseCase;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.functions.Functions;
import io.reactivex.schedulers.Schedulers;

public class MovieDetailsPresenter extends BasePresenter<MovieDetailsContract.View> implements MovieDetailsContract.Presenter {

    private final GetMovieUseCase getMovieUseCase;
    private final FavoriteMovieUseCase favoriteMovieUseCase;
    private final UnfavoriteMovieUseCase unfavoriteMovieUseCase;

    private MovieDetailsContract.MovieViewModel viewModel;

    @Inject
    public MovieDetailsPresenter(final GetMovieUseCase getMovieUseCase,
                                 final FavoriteMovieUseCase favoriteMovieUseCase,
                                 final UnfavoriteMovieUseCase unfavoriteMovieUseCase) {
        this.getMovieUseCase = getMovieUseCase;
        this.favoriteMovieUseCase = favoriteMovieUseCase;
        this.unfavoriteMovieUseCase = unfavoriteMovieUseCase;
    }

    @Override
    public void favorite(final boolean isFavorite, final String movieId) {
        view.animateFavoriteAction();
        if (isFavorite) {
            addDisposable(unfavoriteMovieUseCase.execute(movieId)
                                                .subscribeOn(Schedulers.io())
                                                .subscribe(Functions.EMPTY_ACTION, Throwable::printStackTrace));
        } else {
            addDisposable(favoriteMovieUseCase.execute(view.getMovieId())
                                              .subscribeOn(Schedulers.io())
                                              .subscribe(Functions.EMPTY_ACTION, Throwable::printStackTrace));
        }
    }

    @Override
    public void imageClicked() {
        if (viewModel.isExpanded()) {
            viewModel = new MovieDetailsContract.MovieViewModel(viewModel.getMovie(), false);
        } else {
            viewModel = new MovieDetailsContract.MovieViewModel(viewModel.getMovie(), true);
        }
        view.render(viewModel);
    }

    @Override
    public void onStart() {
        addDisposable(getMovieUseCase.run(view.getMovieId())
                                     .subscribeOn(Schedulers.io())
                                     .map(movie -> new MovieDetailsContract.MovieViewModel(movie, false))
                                     .observeOn(AndroidSchedulers.mainThread())
                                     .subscribe(viewModel -> {
                                         this.viewModel = viewModel;
                                         view.render(this.viewModel);
                                     }, Throwable::printStackTrace));
    }

    @Override
    public void onStop() {

    }
}
