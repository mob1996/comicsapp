package com.five.comicsapp.ui.moviedetails.details;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import com.five.comicsapp.di.scopes.ActivityScope;
import com.five.comicsapp.di.scopes.FragmentScope;
import com.five.comicsapp.util.Constants;
import com.five.domain.usecases.impl.FavoriteMovieUseCase;
import com.five.domain.usecases.impl.GetMovieUseCase;
import com.five.domain.usecases.impl.UnfavoriteMovieUseCase;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MovieDetailsModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract MovieDetailsFragment movieDetailsFragment();

    @ActivityScope
    @Provides
    static FragmentManager fragmentManager(final MovieDetailsActivity movieDetailsActivity) {
        return movieDetailsActivity.getSupportFragmentManager();
    }

    @ActivityScope
    @Provides
    static ActionBar actionBar(final MovieDetailsActivity movieDetailsActivity) {
        return movieDetailsActivity.getSupportActionBar();
    }

    @ActivityScope
    @Provides
    static String provideMovieId(final MovieDetailsActivity movieDetailsActivity) {
        return movieDetailsActivity.getIntent().getStringExtra(Constants.IntentKeys.EXTRA_MOVIE_ID);
    }

    @ActivityScope
    @Binds
    abstract MovieDetailsContract.Presenter movieDetailsPresenter(final MovieDetailsPresenter movieDetailsPresenter);
}
