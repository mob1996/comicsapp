package com.five.comicsapp.ui.frontpage.holder;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.five.comicsapp.ui.frontpage.favorite.FrontPageFavoriteFragment;
import com.five.comicsapp.ui.frontpage.movies.FrontPageFragment;

public class FrontPagePagerAdapter extends FragmentPagerAdapter {

    private static int PAGES_NUM = 2;

    private FrontPageFragment frontPageFragment;
    private FrontPageFavoriteFragment frontPageFavoriteFragment;

    public FrontPagePagerAdapter(final FragmentManager fm, final FrontPageFragment frontPageFragment,
                                 final FrontPageFavoriteFragment frontPageFavoriteFragment) {
        super(fm);
        this.frontPageFragment = frontPageFragment;
        this.frontPageFavoriteFragment = frontPageFavoriteFragment;
    }

    @Override
    public Fragment getItem(final int position) {
        switch (position) {
            case 0:
                return frontPageFragment;
            case 1:
                return frontPageFavoriteFragment;
        }
        return frontPageFragment;
    }

    @Override
    public int getCount() {
        return PAGES_NUM;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(final int position) {
        switch (position) {
            case 0:
                return "Movies";
            case 1:
                return "Favorites";
            default:
                return null;
        }
    }
}
