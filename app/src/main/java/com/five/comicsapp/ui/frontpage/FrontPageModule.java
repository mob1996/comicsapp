package com.five.comicsapp.ui.frontpage;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import com.five.comicsapp.di.scopes.ActivityScope;
import com.five.comicsapp.di.scopes.FragmentScope;
import com.five.comicsapp.ui.frontpage.favorite.FrontPageFavoriteContract;
import com.five.comicsapp.ui.frontpage.favorite.FrontPageFavoriteFragment;
import com.five.comicsapp.ui.frontpage.favorite.FrontPageFavoritePresenter;
import com.five.comicsapp.ui.frontpage.movies.FrontPageContract;
import com.five.comicsapp.ui.frontpage.movies.FrontPageFragment;
import com.five.comicsapp.ui.frontpage.movies.FrontPagePresenter;
import com.five.domain.usecases.impl.FavoriteMovieUseCase;
import com.five.domain.usecases.impl.FetchMoviesUseCase;
import com.five.domain.usecases.impl.GetFavoriteMoviesUseCase;
import com.five.domain.usecases.impl.GetTrendingMoviesUseCase;
import com.five.domain.usecases.impl.UnfavoriteMovieUseCase;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FrontPageModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract FrontPageFragment frontPageFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract FrontPageFavoriteFragment frontPageFavoriteFragment();

    @ActivityScope
    @Provides
    static FragmentManager fragmentManager(final FrontPageActivity frontPageActivity) {
        return frontPageActivity.getSupportFragmentManager();
    }

    @ActivityScope
    @Provides
    static ActionBar actionBar(final FrontPageActivity frontPageActivity) {
        return frontPageActivity.getSupportActionBar();
    }

    @ActivityScope
    @Binds
    abstract FrontPageContract.Presenter frontPagePresenter(final FrontPagePresenter frontPagePresenter);

    @ActivityScope
    @Binds
    abstract FrontPageFavoriteContract.Presenter frontPageFavoritePresenter(final FrontPageFavoritePresenter frontPageFavoritePresenter);
}

