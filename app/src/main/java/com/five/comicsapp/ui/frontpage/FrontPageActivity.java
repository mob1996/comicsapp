package com.five.comicsapp.ui.frontpage;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.five.comicsapp.R;
import com.five.comicsapp.ui.frontpage.favorite.FrontPageFavoriteFragment;
import com.five.comicsapp.ui.frontpage.holder.FrontPagePagerAdapter;
import com.five.comicsapp.ui.frontpage.movies.FrontPageFragment;
import com.five.comicsapp.ui.search.SearchActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import dagger.android.support.DaggerAppCompatActivity;

public class FrontPageActivity extends DaggerAppCompatActivity {

    @BindView(R.id.frontPageViewPager)
    ViewPager viewPager;

    @BindView(R.id.frontPagePagerTabStrip)
    PagerTabStrip pagerTabStrip;

    @Inject
    FragmentManager fragmentManager;

    @Inject
    FrontPageFragment frontPageFragment;

    @Inject
    FrontPageFavoriteFragment frontPageFavoriteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final FrontPagePagerAdapter frontPagePagerAdapter = new FrontPagePagerAdapter(fragmentManager,
                                                                                      frontPageFragment,
                                                                                      frontPageFavoriteFragment);
        viewPager.setAdapter(frontPagePagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_front_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
