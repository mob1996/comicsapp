package com.five.comicsapp.ui.addeditnote;

import com.five.comicsapp.base.BasePresenter;
import com.five.domain.usecases.impl.AddMovieNoteUseCase;

import javax.inject.Inject;

public class AddEditNotePresenter extends BasePresenter<AddEditNoteContract.View> implements AddEditNoteContract.Presenter {

    private final AddMovieNoteUseCase addMovieNoteUseCase;

    @Inject
    public AddEditNotePresenter(final AddMovieNoteUseCase addMovieNoteUseCase) {
        this.addMovieNoteUseCase = addMovieNoteUseCase;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
