package com.five.comicsapp.util;

public interface Consumer<T> {

    void accept(T t);
}