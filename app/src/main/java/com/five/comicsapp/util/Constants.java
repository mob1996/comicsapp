package com.five.comicsapp.util;

public class Constants {

    public static final String API_KEY = "2e7a75541721af0132f5c1c7729619708a8c3438";
    public static final String COMIC_VINE_URL = "https://comicvine.gamespot.com/";

    public static final int SCROLL_UP = -1;
    public static final int SCROLL_DOWN = 1;

    public static class IntentKeys {

        public static final String EXTRA_MOVIE_ID = "movie_id";
    }
}
