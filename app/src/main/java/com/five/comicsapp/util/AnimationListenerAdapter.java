package com.five.comicsapp.util;

import android.view.animation.Animation;

public class AnimationListenerAdapter implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(final Animation animation) {

    }

    @Override
    public void onAnimationEnd(final Animation animation) {

    }

    @Override
    public void onAnimationRepeat(final Animation animation) {

    }
}
