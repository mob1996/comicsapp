package com.five.comicsapp.di.application;

import android.app.Application;

import android.content.Context;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class ApplicationModule {

    @Binds
    abstract Context context(Application application);
}

