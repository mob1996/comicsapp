package com.five.comicsapp.di.application;

import com.five.domain.repository.MovieRepository;
import com.five.domain.usecases.impl.AddMovieNoteUseCase;
import com.five.domain.usecases.impl.DeleteMovieNoteUseCase;
import com.five.domain.usecases.impl.FavoriteMovieUseCase;
import com.five.domain.usecases.impl.FetchMovieUseCase;
import com.five.domain.usecases.impl.FetchMoviesUseCase;
import com.five.domain.usecases.impl.GetFavoriteMoviesUseCase;
import com.five.domain.usecases.impl.GetMovieNoteUseCase;
import com.five.domain.usecases.impl.GetMovieUseCase;
import com.five.domain.usecases.impl.GetTrendingMoviesUseCase;
import com.five.domain.usecases.impl.SearchMoviesUseCase;
import com.five.domain.usecases.impl.UnfavoriteMovieUseCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UseCaseModule {

    @Provides
    @Singleton
    GetTrendingMoviesUseCase getTrendingMoviesUseCase(final MovieRepository movieRepository) {

        return new GetTrendingMoviesUseCase(movieRepository);
    }

    @Provides
    @Singleton
    FavoriteMovieUseCase favoriteMovieUseCase(final MovieRepository movieRepository) {

        return new FavoriteMovieUseCase(movieRepository);
    }

    @Provides
    @Singleton
    UnfavoriteMovieUseCase unfavoriteMovieUseCase(final MovieRepository movieRepository) {
        return new UnfavoriteMovieUseCase(movieRepository);
    }

    @Provides
    @Singleton
    FetchMoviesUseCase fetchMoviesUseCase(final MovieRepository movieRepository) {
        return new FetchMoviesUseCase(movieRepository);
    }

    @Provides
    @Singleton
    GetMovieUseCase getMovieUseCase(final MovieRepository movieRepository) {
        return new GetMovieUseCase(movieRepository);
    }

    @Provides
    @Singleton
    SearchMoviesUseCase searchMoviesUseCase(final MovieRepository movieRepository) {

        return new SearchMoviesUseCase(movieRepository);
    }

    @Provides
    @Singleton
    FetchMovieUseCase fetchMovieUseCase(final MovieRepository movieRepository) {
        return new FetchMovieUseCase(movieRepository);
    }

    @Provides
    @Singleton
    GetFavoriteMoviesUseCase getFavoriteMoviesUseCase(final MovieRepository movieRepository) {
        return new GetFavoriteMoviesUseCase(movieRepository);
    }

    @Provides
    @Singleton
    AddMovieNoteUseCase addMovieNoteUseCase(final MovieRepository movieRepository) {
        return new AddMovieNoteUseCase(movieRepository);
    }

    @Provides
    @Singleton
    DeleteMovieNoteUseCase deleteMovieNoteUseCase(final MovieRepository movieRepository) {
        return new DeleteMovieNoteUseCase(movieRepository);
    }

    @Provides
    @Singleton
    GetMovieNoteUseCase getMovieNotesUseCase(final MovieRepository movieRepository) {
        return new GetMovieNoteUseCase(movieRepository);
    }
}
