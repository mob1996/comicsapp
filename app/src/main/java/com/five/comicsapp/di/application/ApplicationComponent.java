package com.five.comicsapp.di.application;

import android.app.Application;

import com.five.comicsapp.application.ComicsApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {ApplicationModule.class,
                      ActivityBindingModule.class,
                      NetworkingModule.class,
                      UseCaseModule.class,
                      AndroidSupportInjectionModule.class,
                      DatabaseModule.class})
public interface ApplicationComponent extends AndroidInjector<ComicsApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        ApplicationComponent build();
    }
}
