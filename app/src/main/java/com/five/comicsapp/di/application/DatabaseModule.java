package com.five.comicsapp.di.application;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.five.data.MovieRepositoryImpl;
import com.five.data.local.movie.MovieDao;
import com.five.data.local.movie.MovieDatabase;
import com.five.data.remote.movie.MovieService;
import com.five.domain.repository.MovieRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Singleton
    @Provides
    MovieDatabase movieDatabase(final Context applicationContext) {
        return Room.databaseBuilder(applicationContext,
                                    MovieDatabase.class,
                                    "movie-database")
                   .build();
    }

    @Singleton
    @Provides
    MovieDao movieDao(final MovieDatabase movieDatabase) {
        return movieDatabase.movieDao();
    }

    @Singleton
    @Provides
    MovieRepository movieRepository(final MovieDao movieDao, final MovieService movieService) {
        return new MovieRepositoryImpl(movieService, movieDao);
    }
}
