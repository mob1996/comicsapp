package com.five.comicsapp.di.application;

import com.five.comicsapp.di.scopes.ActivityScope;
import com.five.comicsapp.ui.addeditnote.AddEditNoteActivity;
import com.five.comicsapp.ui.addeditnote.AddEditNoteModule;
import com.five.comicsapp.ui.frontpage.FrontPageActivity;
import com.five.comicsapp.ui.frontpage.FrontPageModule;
import com.five.comicsapp.ui.moviedetails.details.MovieDetailsActivity;
import com.five.comicsapp.ui.moviedetails.details.MovieDetailsModule;
import com.five.comicsapp.ui.search.SearchActivity;
import com.five.comicsapp.ui.search.SearchModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = FrontPageModule.class)
    abstract FrontPageActivity frontPageActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = MovieDetailsModule.class)
    abstract MovieDetailsActivity movieDetailsActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = SearchModule.class)
    abstract SearchActivity searchActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = AddEditNoteModule.class)
    abstract AddEditNoteActivity addEditNoteActivity();
}
