package com.five.comicsapp.di.application;

import com.five.comicsapp.util.Constants;
import com.five.data.remote.movie.MovieService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkingModule {

    @Singleton
    @Provides
    Gson gson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
    }

    @Singleton
    @Provides
    OkHttpClient httpClient() {
        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    final Request originalRequest = chain.request();

                    final HttpUrl originalHttpUrl = originalRequest.url();
                    final HttpUrl newUrl = originalHttpUrl.newBuilder()
                                                          .addQueryParameter("api_key", Constants.API_KEY)
                                                          .addQueryParameter("format", "json")
                                                          .build();
                    final Request newRequest = originalRequest.newBuilder().url(newUrl).build();
                    return chain.proceed(newRequest);
                })
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Singleton
    @Provides
    Retrofit retrofit(final Gson gson, final OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.COMIC_VINE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build();
    }

    @Singleton
    @Provides
    MovieService movieService(final Retrofit retrofit) {
        return retrofit.create(MovieService.class);
    }
}
