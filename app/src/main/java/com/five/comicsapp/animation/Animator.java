package com.five.comicsapp.animation;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;

import com.five.comicsapp.util.AnimationListenerAdapter;

public class Animator {

    public static void popAnimation(final View view, final float scale, final int halfDurationMs) {
        final Animation scaleUp = new ScaleAnimation(1.0f, scale,
                                                     1.0f, scale,
                                                     Animation.RELATIVE_TO_SELF, 0.0f,
                                                     Animation.RELATIVE_TO_SELF, 0.0f);
        scaleUp.setFillAfter(true);
        scaleUp.setDuration(halfDurationMs);

        final Animation scaleDown = new ScaleAnimation(scale, 1.0f,
                                                       scale, 1.0f,
                                                       Animation.RELATIVE_TO_SELF, 0.0f,
                                                       Animation.RELATIVE_TO_SELF, 0.0f);
        scaleDown.setFillAfter(true);
        scaleDown.setDuration(halfDurationMs);

        scaleUp.setAnimationListener(new AnimationListenerAdapter() {

            @Override
            public void onAnimationEnd(final Animation animation) {
                view.startAnimation(scaleDown);
            }
        });

        view.startAnimation(scaleUp);
    }

    public static void scale(final View view, final float scale, final int durationMs) {
        final Animation scaleAni = new ScaleAnimation(1.0f, scale,
                                                      1.0f, scale,
                                                      Animation.RELATIVE_TO_SELF, 0.0f,
                                                      Animation.RELATIVE_TO_SELF, 0.0f);
        scaleAni.setFillAfter(true);
        scaleAni.setDuration(durationMs);
        view.startAnimation(scaleAni);
    }

    public static void scale(final View view, final float startScale, final float scale, final int durationMs) {
        final Animation scaleAni = new ScaleAnimation(startScale, scale,
                                                      startScale, scale,
                                                      Animation.RELATIVE_TO_SELF, 0.0f,
                                                      Animation.RELATIVE_TO_SELF, 0.0f);
        scaleAni.setFillAfter(true);
        scaleAni.setDuration(durationMs);
        view.startAnimation(scaleAni);
    }


    public static void rotate(final View view, final float startAngle, final float endAngle, final int animationTime) {
        final Animation rotate = new RotateAnimation(startAngle, endAngle,
                                                        Animation.RELATIVE_TO_SELF, 0.5f,
                                                        Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(animationTime);
        view.startAnimation(rotate);
    }
}
