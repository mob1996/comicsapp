package com.five.comicsapp.application;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

import com.facebook.stetho.Stetho;
import com.five.comicsapp.di.application.DaggerApplicationComponent;

public class ComicsApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder()
                                         .application(this)
                                         .build();
    }
}
