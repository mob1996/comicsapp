package com.five.comicsapp.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<View> implements ViewPresenter<View> {

    protected View view;

    private final CompositeDisposable disposables = new CompositeDisposable();

    public abstract void onStart();

    public abstract void onStop();

    @Override
    public void start(final View view) {
        this.view = view;
        onStart();
    }

    @Override
    public void stop() {
        disposables.clear();
        this.view = null;
        onStop();
    }

    protected void addDisposable(final Disposable disposable) {
        disposables.add(disposable);
    }
}
