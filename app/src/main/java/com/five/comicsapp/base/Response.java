package com.five.comicsapp.base;

import com.annimon.stream.Optional;

import lombok.Value;

@Value
public class Response<T> {

    public static <T> Response withValue(final T value) {
        return new Response<>(Optional.of(value), Optional.empty());
    }

    public static Response withError(final Throwable throwable) {
        return new Response<>(Optional.empty(), Optional.of(throwable));
    }

    private final Optional<T> value;
    private final Optional<Throwable> throwable;
}
