package com.five.comicsapp.base;

public interface ViewPresenter<View> {

    void start(View view);

    void stop();
}
